// src/app/translation.service.ts
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
type Direction = 'ltr' | 'rtl';
type Lang = 'en' | 'ar';

@Injectable({
  providedIn: 'root',
})
export class TranslationService {
  constructor(private translate: TranslateService) { }

  init() {
    const lang = (localStorage.getItem('lang') || 'ar') as Lang;
    const direction = (localStorage.getItem('direction') || 'rtl') as Direction;
    document.documentElement.dir = direction;
    document.documentElement.lang = lang;
    this.translate.addLangs(['en', 'ar']);
    this.translate.setDefaultLang(lang);
    this.translate.use(lang);
    this.setDirection(direction, lang);
  }


  changeLanguage(lang: Lang) {
    this.translate.use(lang);
    const direction: Direction = lang == 'en' ? 'ltr' : 'rtl';
    localStorage.setItem('lang', lang);
    localStorage.setItem('direction', direction);
    this.setDirection(direction, lang);
  }
  getTransValue(name: string) {
    let message: string = ''
    this.translate.get(name).subscribe(res => message = res)
    return message
  }

  private setDirection(direction: Direction, lang: Lang = 'ar') {
    document.documentElement.dir = direction;
    document.documentElement.lang = lang;
  }
  get lang() {
    return this.currentLang == 'ar' ? 'en' : 'ar' as const
  }
  get currentLang(): Lang {
    return this.translate.currentLang as Lang;
  }
  get direction(): Direction {
    return this.currentLang === 'ar' ? 'rtl' : 'ltr';
  }
}
