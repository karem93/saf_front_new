import { Component, } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';


@Component({
  selector: 'app-payment-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet],
  templateUrl: './payment.layout.html',

})
export class PaymentLayout {
  title = 'payment';



}
