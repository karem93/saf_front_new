import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationEnd, Router, RouterOutlet } from '@angular/router';
import { TopBarComponent } from 'app/components/top-bar/top-bar.component';
import { fadeAnimation } from 'app/animations/fade';
import { FloatingButtonComponent } from 'app/components/floating-button/floating-button.component';
import { FooterComponent } from 'app/components/footer/footer.component';
import { NavBarComponent } from 'app/components/nav-bar/nav-bar.component';
import { SnackBarComponent } from 'app/components/snack-bar/snack-bar.component';
import { filter } from 'rxjs';
import { TranslationService } from 'assets/i18n/translation.service';


@Component({
  selector: 'app-default-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, TopBarComponent, NavBarComponent, FooterComponent, FloatingButtonComponent, SnackBarComponent],
  templateUrl: './default.layout.html',

  animations: [
    fadeAnimation
  ],
})
export class DefaultLayout implements OnInit {
  title = 'saf-project';
  constructor(private transService: TranslationService, private router: Router, private cdr: ChangeDetectorRef) {

  }

  ngOnInit() {
    this.transService.init()
    this.scrollToTop()
    this.cdr.detectChanges()

  }

  scrollToTop() {
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(() => {
        window.scrollTo({ top: 0, behavior: 'smooth' });
      });
  }

}
