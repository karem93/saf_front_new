export interface ContactInfo {
  address: string;
  contact_email: string;
  phone: string;
  facebook_link: string;
  twitter_link: string;
  snapchat_link: string;
  instagram_link: string;
  youtube_link: string;
  latitude: number;
  longitude: number;
  website_name: string;
  website_logo: string;
  website_bio: string;
  [key: string]: any
}


interface Base {
  id: number;
  img: string;
}
export interface Slider extends Base {
  link: string;
}
export interface Section extends Base {
  name: string;
}
export interface Country {
  id: number;
  name: string;
  flag: string;
  phone_code: string;
}
export interface Product {
  bio: string;
  cover_img: string;
  flag: number;
  id: number;
  name: string;
  SAR_price: number;
  SAR_price_after: number;
  SAR_discount: number;
  USD_discount: number;
  USD_price: number;
  USD_price_after: number;
  meta_description: string;
}

export interface StoreType {
  id: number;
  name: string;
  image: string;
  country: Country;
}

export interface Review {
  img: string;
  id: number;
  name: string;
  job_name: string;
  message: string;
  created_at: string;
}
export interface StaticPage {
  content: string;
  id: 1;
  image: string;
  title: string;
}


export interface HomeData {
  sliders: Slider[];
  sections: Section[];
  stores: StoreType[];
  reviews: Review[];
  staticpage: StaticPage;
}
