import { Injectable, signal } from '@angular/core';
import { ErrorType, Status, messageStatusColor } from './errorTypes';
@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  error = signal<ErrorType>({ message: 'this is error message for test', color: "bg-red-500" })
  private showMessage = false
  constructor() {
  }

  setError(message: string, status: Status) {
    this.showMessage = true
    this.error().message = message
    this.error().color = messageStatusColor[status]
    setTimeout(() => {
      this.showMessage = false
    }, 3000)
  }

  get show(): boolean {
    return this.showMessage
  }

}
