import { HttpClient } from '@angular/common/http';
import { Injectable, effect, signal } from '@angular/core';
import { ContactInfo, HomeData } from './types';
import { Observable, map, of, shareReplay, switchMap } from 'rxjs';
import { toSignal } from '@angular/core/rxjs-interop'
@Injectable({
  providedIn: 'root'
})
export class HomeService {

  contactInfo$: Observable<ContactInfo> = new Observable()
  homeData$: Observable<HomeData> = new Observable()
  constructor(private http: HttpClient) {
    this.fetchContactInfo()
    this.fetchHomepageInfo()
  }

  fetchContactInfo() {
    this.contactInfo$ = this.http.get<{ data: ContactInfo[] }>('contactInfo').pipe(
      shareReplay(),
      map(res => res.data),
      map(data => {
        return data.reduce((acc, value) => {
          return { ...acc, ...Object.fromEntries(Object.entries(value)) }
        }, {}) as ContactInfo
      }),
    )
  }
  fetchHomepageInfo() {
    this.homeData$ = this.http.get<{ data: HomeData }>('homepage').pipe(
      shareReplay(),
      map(res => res.data)

    )
  }



}
