export interface ErrorType {
  message: string;
  color: string;
}

export type Status = 'success' | 'error'

export enum messageStatusColor {
  success = "bg-green-500",
  error = "bg-red-500",
}
