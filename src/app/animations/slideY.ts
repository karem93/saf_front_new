import { animate, state, style, transition, trigger } from "@angular/animations";


export const slideYAnimation = trigger('slideYAnimation', [
  state(
    'in',
    style({
      opacity: 1,
    })
  ),
  transition('void=>*', [
    style({
      opacity: 0,
      transform: 'translateY(-10px)',
    }),
    animate(200),
  ]),
  transition('*=>void', [
    animate(
      200,
      style({
        opacity: 0,
        transform: 'translateY(-10px)',
      })
    ),
  ]),
])
