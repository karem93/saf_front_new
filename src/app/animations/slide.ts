import { animate, style, transition, trigger } from "@angular/animations";

export const slideAnimation = trigger('slideAnimation', [
  transition('* => *', [
    style({ transform: 'translateX(100%)', opacity: 0 }),
    animate('300ms', style({ transform: 'translateX(0%)', opacity: 1 })),
  ]),

])
