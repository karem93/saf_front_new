import { animate, style, transition, trigger } from "@angular/animations";

export const fadeAnimation = trigger('fadeAnimation', [
  transition('* <=> *', [
    style({ opacity: 0, filter: 'blur(1rem)' }),
    animate('500ms', style({ opacity: 1, filter: 'blur(0)' })),
  ]),
])
