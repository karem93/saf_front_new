import { ApplicationConfig, importProvidersFrom } from '@angular/core';
import { provideRouter, withHashLocation } from '@angular/router';

import { routes } from './app.routes';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslationService } from '../assets/i18n/translation.service';
import { HttpConfigInterceptor } from './config/http-config.interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


export const appConfig: ApplicationConfig = {
  // withHashLocation()
  providers: [provideRouter(routes),
    TranslationService,
  importProvidersFrom(
    HttpClientModule,
    BrowserAnimationsModule,
    TranslateModule.forRoot({
      defaultLanguage: "en",
      isolate: true,
      loader: {

        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],

      },
    })
  ),
  { provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true }
  ],

};
