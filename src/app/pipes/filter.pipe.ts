import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter',
  standalone: true
})
export class FilterPipe implements PipeTransform {

  transform(items: any[], filterKey: string, filterValue: string): any[] {
    return items.filter(item => (item[filterKey] as string)?.trim().match(filterValue.trim()))
  }

}
