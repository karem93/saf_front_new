import { HttpErrorResponse, HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Observable, catchError, tap, throwError } from 'rxjs';
import { ErrorService } from 'app/store/error.service';
import { inject } from '@angular/core';

export class HttpConfigInterceptor implements HttpInterceptor {
  private errorService: ErrorService = inject(ErrorService)
  lang = localStorage.getItem('lang') || 'ar'
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.set('Accept-Language', this.lang);

    if (!req.url.includes('/assets/i18n/')) {
      const url = environment.baseUrl + req.url;
      const modifiedRequest = req.clone({ url, headers });

      return next.handle(modifiedRequest).pipe(
        tap((event) => {
          if (event instanceof HttpResponse) {
            const statusCode = event.status;
            if (statusCode >= 200 && event.body && event.body.message) {
              this.errorService.setError(event.body.message, 'success')
            }
          }
        }),
        catchError((error: HttpErrorResponse) => {
          if (error.error && error.error.message) {
            this.errorService.setError(error.error.message, 'error')
          }
          return throwError(() => error);
        })
      );
    }

    return next.handle(req.clone({ headers }));
  }
}
