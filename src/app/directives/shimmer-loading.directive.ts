import { AfterViewInit, Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appShimmerLoading]',
  standalone: true
})
export class ShimmerLoadingDirective implements OnInit {
  @Input() loading: boolean = true;

  constructor(private el: ElementRef<HTMLElement>) { }

  ngOnInit(): void {
    this.showContent();
  }

  ngOnChanges(): void {
    this.showContent();
  }

  private showContent(): void {
    if (this.loading) {
      this.el.nativeElement.classList.add('shimmer-loading');
    } else {
      this.el.nativeElement.classList.remove('shimmer-loading');
    }
  }

}
