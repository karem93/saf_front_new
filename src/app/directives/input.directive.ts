import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appInput]',
  standalone: true
})
export class InputDirective {

  constructor(private el: ElementRef<HTMLInputElement>) {
    this.el.nativeElement.classList.add('input')

  }

}
