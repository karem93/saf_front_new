import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[InputField]',
  standalone: true
})
export class InputFieldDirective {

  constructor(private el: ElementRef<HTMLInputElement>) {
    this.el.nativeElement.classList.add('input-field')

  }

}
