import { Routes } from "@angular/router";

export const mainRoutes: Routes = [
  {
    path: '',
    loadComponent: () => import('app/layouts/default/default.layout').then(m => m.DefaultLayout),
    children: [
      { path: '', loadComponent: () => import('app/pages/home/home.component').then(m => m.HomeComponent) },
      { path: 'sections', loadComponent: () => import('app/pages/sections/sections.component').then((m) => m.SectionsComponent) },
      { path: 'presenters/:id/:storeName', loadComponent: () => import('app/pages/presenters/presenters.component').then(m => m.PresentersComponent) },
      { path: 'presenters/:id/details/:storeId', loadComponent: () => import('app/pages/presenters/details/details.component').then(m => m.DetailsComponent) },
      { path: 'latest-store', loadComponent: () => import('app/pages/latest-store/latest-store.component').then(m => m.LatestStoreComponent) },
      { path: 'subscription', loadComponent: () => import('app/pages/subscription/subscription.component').then(m => m.SubscriptionComponent) },
      { path: 'subscription/complete/:id', loadComponent: () => import('app/pages/subscription/complete/complete.component').then(m => m.CompleteComponent) },
      { path: 'subscription/packages/:userId', loadComponent: () => import('app/pages/subscription/packages/packages.component').then(m => m.PackagesComponent) },
      { path: 'subscription/checkout/:userId/:packId', loadComponent: () => import('app/pages/subscription/checkout/checkout.component').then(m => m.CheckoutComponent) },

      { path: 'subscription/successfully', loadComponent: () => import('app/pages/subscription/successfully/successfully.component').then(m => m.SuccessfullyComponent) },
      { path: 'common-questions', loadComponent: () => import('app/pages/common-questions/common-questions.component').then(m => m.CommonQuestionsComponent) },
      { path: 'about-us', loadComponent: () => import('app/pages/about-us/about-us.component').then(m => m.AboutUsComponent) },
      { path: 'privacy-policy', loadComponent: () => import('app/pages/privacy-policy/privacy-policy.component').then(m => m.PrivacyPolicyComponent) },
      { path: 'condition-for-replacement', loadComponent: () => import('app/pages/condition-for-replacement/condition-for-replacement.component').then(m => m.ConditionForReplacementComponent) },
      { path: 'contact-us', loadComponent: () => import('app/pages/contact-us/contact-us.component').then(m => m.ContactUsComponent) },

    ]
  }
]
