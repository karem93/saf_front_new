import { Routes } from "@angular/router";

export const paymentRoutes: Routes = [
  {
    path: 'subscription',
    loadComponent: () => import('app/layouts/payment/payment.layout').then(m => m.PaymentLayout),
    children: [
      { path: 'payment/:subId', loadComponent: () => import('app/pages/subscription/payment/payment.component').then(m => m.PaymentComponent) },
      { path: 'checkout-status', loadComponent: () => import('app/pages/subscription/checkout-status/checkout-status.component').then(m => m.CheckoutStatusComponent) },
    ]
  }
]
