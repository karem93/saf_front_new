import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";
import { emailPattern } from "app/utils/patterns";

export function emailValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {

    if (!emailPattern.test(control.value)) {
      return { invalidEmail: true };
    }
    return null;
  };
}


