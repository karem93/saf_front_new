import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export function oneOfValidator(validOptions: string[]): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if (!validOptions.includes(control.value)) {
      return { invalidOption: true };
    }
    return null;
  };
}


