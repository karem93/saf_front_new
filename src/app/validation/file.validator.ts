import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export function validateImage(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const value: File = control.value
    const validExtensions = /\.(png|jpeg|jpg)$/i;
    const isValidImage = value && validExtensions.test(value.name)
    return isValidImage ? null : { inValidImage: true }
  }
}