import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";
import { phoneValidation } from "app/utils/patterns";

export function phoneValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {

    if (!phoneValidation.test(control.value)) {
      return { invalidPhoneNumber: true };
    }
    return null;
  };
}


