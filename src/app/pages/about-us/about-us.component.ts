import { Component } from '@angular/core';
import { StaticPageComponent } from 'app/components/static-page/static-page.component';

@Component({
  selector: 'app-about-us',
  standalone: true,
  imports: [StaticPageComponent],
  templateUrl: './about-us.component.html',
  styleUrl: './about-us.component.scss'
})
export class AboutUsComponent {

}
