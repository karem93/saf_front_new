export interface ISection {
  id: number;
  img: string;
  name: string
}

export interface MetaOption {
  current_page: number;
  last_page: number;
}
