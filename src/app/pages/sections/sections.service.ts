import { HttpClient } from "@angular/common/http";
import { Injectable, inject } from "@angular/core";
import { ISection } from "./types";
import { map } from "rxjs";

@Injectable({ providedIn: 'root' })
export class SectionsService {
  private http: HttpClient = inject(HttpClient)


  fetchSections() {
    return this.http.get<{ data: ISection[] }>('sections').pipe(
      map((res) => {
        const { data } = res
        return data
      })
    )
  }

}
