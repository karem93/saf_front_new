import { AfterContentChecked, AfterContentInit, AfterViewChecked, AfterViewInit, Component, ElementRef, OnChanges, OnInit, ViewChild, inject } from '@angular/core';
import { PageTitleComponent } from 'app/components/shared/page-title/page-title.component';
import { SliderComponent } from 'app/components/shared/slider/slider.component';
import { SectionsService } from './sections.service';
import { ISection, MetaOption } from './types';
import { Observable, map } from 'rxjs';
import { CommonModule } from '@angular/common';
import { BreakPoint } from 'app/components/shared/slider/model';
import { SectionCardComponent } from 'app/components/section-card/section-card.component';
import { StoresComponent } from 'app/components/home/stores/stores.component';
import { HttpClient } from '@angular/common/http';
import { StoreType } from 'app/store/types';
import { TranslateModule } from '@ngx-translate/core';
import { ButtonComponent } from 'app/components/shared/button/button.component';
import { LoaderComponent } from 'app/components/shared/loader/loader.component';
import { ShimmerLoadingDirective } from 'app/directives/shimmer-loading.directive';

@Component({
  selector: 'app-sections',
  standalone: true,
  imports: [PageTitleComponent, SliderComponent, CommonModule, SectionCardComponent, StoresComponent, TranslateModule, ButtonComponent, LoaderComponent, ShimmerLoadingDirective],
  templateUrl: './sections.component.html',
  styleUrl: './sections.component.scss'
})
export class SectionsComponent implements OnInit, AfterViewInit {
  @ViewChild('sectionCard') sectionCard!: SectionCardComponent
  sections$!: Observable<ISection[]>
  stores: StoreType[] = []
  loading: boolean = false
  loadingData: boolean = false
  metaOption!: MetaOption
  activeSection!: ISection
  breakPoints: BreakPoint = {
    320: {
      slidesPerView: 1,
      spaceBetween: 10,
    },
    480: {
      slidesPerView: 2,
      spaceBetween: 10,

    },
    1200: {
      slidesPerView: 4,
      spaceBetween: 10,

    },
    1536: {
      slidesPerView: 8,
      spaceBetween: 1,

    },

  }
  constructor(private sectionService: SectionsService, private http: HttpClient) { }
  ngOnInit(): void {
    this.sections$ = this.sectionService.fetchSections()
  }

  onSelectSection(item: ISection) {
    if (this.activeSection?.id !== item.id) {
      this.activeSection = item;
      this.stores = []; // Clear existing stores
      this.fetchStoresBySection();
    }
  }
  fetchStoresBySection(page: number = 1) {
    this.loading = true;

    this.http.get<{ data: StoreType[], meta: MetaOption }>(`section_stores/${this.activeSection.id}`, { params: { page } })
      .subscribe(res => {
        this.stores = [...this.stores, ...res.data];
        this.loading = false;
        this.metaOption = res.meta
      });
  }

  loadMore() {
    this.fetchStoresBySection(this.metaOption.current_page + 1)
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      if (this.sectionCard) {
        this.onSelectSection(this.sectionCard.item)
      }
    }, 1000);

  }
}
