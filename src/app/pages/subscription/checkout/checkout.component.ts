import { Component, OnDestroy, OnInit } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { BreadCrump } from 'app/components/shared/page-title/model';
import { PageTitleComponent } from 'app/components/shared/page-title/page-title.component';
import { ICupon, IPaymentData, IPaymentMethod, IPaymentProcess } from './types';
import { InputFieldDirective } from 'app/directives/input-field.directive';
import { ButtonComponent } from 'app/components/shared/button/button.component';
import { FormsModule } from '@angular/forms';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { IPackage } from '../packages/types';
import { Subscription, catchError, map, throwError } from 'rxjs';
import { LoadingSpinnerComponent } from 'app/components/shared/loading-spinner/loading-spinner.component';

@Component({
  selector: 'app-checkout',
  standalone: true,
  imports: [PageTitleComponent, TranslateModule, InputFieldDirective, ButtonComponent, FormsModule, LoadingSpinnerComponent],
  templateUrl: './checkout.component.html',
  styleUrl: './checkout.component.scss'
})
export class CheckoutComponent implements OnInit, OnDestroy {
  packageItem!: IPackage
  unsSubPackage!: Subscription
  couponDiscount!: number;
  coupon = ''
  paymentMethod = 'visa'
  loading: boolean = true;
  loadingCoupon: boolean = false;
  coupon_id!: number
  breadCrumbs: BreadCrump[] = [
    { text: 'home', to: '/', active: true },
    { text: 'subscribe_as_presenter', to: '/subscription', active: true },
    { text: 'complete_data', to: '/subscription', active: true },
    { text: 'subscription_and_payment', to: '', active: false },
  ]

  paymentProcessItems: IPaymentProcess[] = [
    { title: 'price_of_registration', valueKey: 'price' },
    { title: 'value_added_taxes', valueKey: 'tax' },
  ]
  paymentMethods: IPaymentMethod[] = [
    { value: 'visa', icon: 'visa.png' },
    { value: 'master', icon: 'master.png' },
    { value: 'paypal', icon: 'paypal.png' },
    { value: 'mada', icon: 'mada.png' },
  ]
  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  showPackage() {
    this.unsSubPackage = this.route.params.subscribe(params => {
      return this.http.get<{ data: IPackage }>(`show_package/${params['packId']}`).pipe(map(res => res.data)).subscribe(data => {
        this.packageItem = { ...data, }
        this.packageItem.total = this.packageItem.price + data.tax
        this.loading = false
      })
    })
  }

  checkCoupon() {
    this.loadingCoupon = true
    this.http.post<{ data: ICupon }>('check_coupon', { code: this.coupon }).pipe(catchError((error: HttpErrorResponse) => {
      this.loadingCoupon = false
      return throwError(() => error);
    }), map(res => ({ ...res.data, amount: parseFloat(res.data.amount) }))).subscribe(data => {
      this.coupon_id = data.id
      if (data.type == 'fixed') {
        this.couponDiscount = data.amount
        this.packageItem.total = this.packageItem.price - this.couponDiscount
      } else {
        const discount = (this.packageItem.total * data.amount) / 100;
        this.couponDiscount = discount

        this.packageItem.total = this.packageItem.price - this.couponDiscount

      }
      this.loadingCoupon = false
    })
  }
  removeCoupon() {
    this.packageItem.total = this.packageItem.total + this.couponDiscount
    this.coupon = ''
    this.couponDiscount = 0
  }
  handlePayment() {
    const data: IPaymentData = {
      user_id: this.route.snapshot.params['userId'],
      package_id: this.route.snapshot.params['packId'],
      store_id: this.route.snapshot.queryParams['store_id'],
      coupon_id: this.coupon_id,
      total_amount: this.packageItem.total
    }
    this.http.post<{ data: { id: number } }>('save_subscription', data).subscribe(({ data }) => {
      this.router.navigate(['/subscription', 'payment', data.id])
    })
  }
  ngOnInit(): void {
    this.showPackage()
  }
  ngOnDestroy(): void {
    this.unsSubPackage.unsubscribe()
  }

}
