export interface IPaymentProcess {
  title: string;
  valueKey: string
}

export interface IPaymentMethod {
  icon: string;
  value: string
}

type TType = 'fixed' | 'percent'

export interface ICupon {
  amount: string;
  code: string;
  type: TType
  id: number;
}

export interface IPaymentData {
  user_id: string;
  package_id: string
  transaction_id?: string;
  coupon_id?: number;
  store_id?: number;
  total_amount: number
}

export interface ISubscriptionRes {
  id: string;
}
