import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ButtonComponent } from 'app/components/shared/button/button.component';
import { FormGroupComponent } from 'app/components/shared/form-group/form-group.component';
import { BreadCrump } from 'app/components/shared/page-title/model';
import { PageTitleComponent } from 'app/components/shared/page-title/page-title.component';
import { IForm } from './types';
import { InputFieldDirective } from 'app/directives/input-field.directive';
import { FileUploadComponent } from 'app/components/shared/file-upload/file-upload.component';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { catchError, of } from 'rxjs';
import { SelectComponent } from 'app/components/shared/select/select.component';
interface IStore {
  name: string;
  id: number
}
@Component({
  selector: 'app-complete',
  standalone: true,
  imports: [PageTitleComponent, TranslateModule, ReactiveFormsModule, FormGroupComponent, ButtonComponent, InputFieldDirective, FileUploadComponent, SelectComponent],
  templateUrl: './complete.component.html',
  styleUrl: './complete.component.scss'
})
export class CompleteComponent {
  loading: boolean = false;
  loadingStores: boolean = true;
  stores: IStore[] = [];
  breadCrumbs: BreadCrump[] = [
    { text: 'home', to: '/', active: true },
    { text: 'subscribe_as_presenter', to: '/subscription', active: true },
    { text: 'complete_data', to: '/subscription', active: false },
  ]

  form!: FormGroup<IForm>;

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private router: Router, private http: HttpClient) {
    this.initializeForm();
    this.getStores()

  }

  private getStores() {
    const country_id = this.route.snapshot.queryParams['country_id']
    const sections = this.route.snapshot.queryParams['sections']
    const data = {
      country_id,
      section_id: sections

    }
    this.http.post<{ data: IStore[] }>(`get_country_stores`, data).subscribe(data => {
      this.stores = data.data
      this.loadingStores = false
    })

  }

  private initializeForm(): void {
    this.form = this.fb.group<IForm>({
      company_name: this.fb.control('', [Validators.required]),
      tax_record: this.fb.control('', [Validators.required]),
      store_id: this.fb.control('', [Validators.required]),
      company_image: this.fb.control(null, [Validators.required]),
      commericalRegister_img: this.fb.control(null, [Validators.required]),

      user_id: this.fb.control(this.route.snapshot.params['id']),
    });
  }

  onSubmit() {
    if (this.form.valid) {
      this.handleSubmit()
    } else {
      this.form.markAllAsTouched()
    }
  }

  generateFormData() {
    const formData = new FormData();
    const { store_id, ...data } = this.form.value
    for (const key in data) {
      formData.append(key, data[key] as string | Blob);
    }
    formData.append('store_id', store_id['id'])
    return formData;
  };
  handleSubmit() {
    this.loading = true
    this.http.post('addPresenter', this.generateFormData()).pipe(
      catchError(err => {
        if (err) {
          this.loading = false
        }
        return of(err)
      })
    ).subscribe((res: { data: { id: number } }) => {
      this.loading = false
      const { data: { id } } = res
      this.router.navigate(['subscription', 'packages', id], {
        queryParams: {
          store_id: this.form.value['store_id']['id']
        }
      })
    })

  }

}
