import { FormControl } from "@angular/forms";

export interface IForm {
  company_name: FormControl<string | null>,
  tax_record: FormControl<string | null>,
  company_image: FormControl<File | null>,
  commericalRegister_img: FormControl<File | null>,
  user_id: FormControl<'' | null>,
  [key: string]: any

}

