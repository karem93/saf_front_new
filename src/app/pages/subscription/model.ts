import { Type } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { FirstStepComponent } from "app/components/subscription/first-step/first-step.component";
import { SecondStepComponent } from "app/components/subscription/second-step/second-step.component";

type Component = FirstStepComponent | SecondStepComponent

export interface IStep {
  component: Type<Component>
  form: FormGroup
}
