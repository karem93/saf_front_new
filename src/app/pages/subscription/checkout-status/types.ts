type TStatus = 'paid' | 'failed'
export interface IQuery {
  message: string;
  status: TStatus;
  id: string;
  subId: string;
  amount: number
}
