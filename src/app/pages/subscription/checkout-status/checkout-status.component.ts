import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IQuery } from './types';
import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-checkout-status',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './checkout-status.component.html',
  styleUrl: './checkout-status.component.scss'
})
export class CheckoutStatusComponent implements OnInit {
  query!: IQuery
  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) {
    this.query = this.route.snapshot.queryParams as IQuery
    console.log(this.query);
  }

  get statusColor(): string {
    if (this.query.status == 'paid') {
      return 'bg-green-200 text-green-700'
    } else {
      return 'bg-red-200 text-red-700'
    }
  }

  handleSubscriptionStatus() {
    const data = {
      transaction_id: this.query.id,
      status: this.query.status == 'paid' ? 'active' : 'cancelled'

    }
    this.http.post(`update_subscription_status/${this.query.subId}`, data).subscribe(() => {
      setTimeout(() => {
        if (this.query.status == 'paid') {
          this.router.navigate(['/subscription', 'successfully'])
        } else {
          this.router.navigate(['/'])

        }
      }, 3000);
    })
  }
  ngOnInit(): void {
    this.handleSubscriptionStatus()
  }
}
