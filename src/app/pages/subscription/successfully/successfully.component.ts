import { Component } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

@Component({
  selector: 'app-successfully',
  standalone: true,
  imports: [TranslateModule],
  templateUrl: './successfully.component.html',
  styleUrl: './successfully.component.scss'
})
export class SuccessfullyComponent {

}
