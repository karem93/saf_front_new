import { Component, OnDestroy, OnInit, inject } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { BreadCrump } from 'app/components/shared/page-title/model';
import { PageTitleComponent } from 'app/components/shared/page-title/page-title.component';
import { PackageCardComponent } from 'app/components/subscription/package-card/package-card.component';
import { PackagesService } from './packages.service';
import { Observable, Subscription } from 'rxjs';
import { IPackage } from './types';

@Component({
  selector: 'app-packages',
  standalone: true,
  imports: [PageTitleComponent, TranslateModule, PackageCardComponent],
  templateUrl: './packages.component.html',
  styleUrl: './packages.component.scss'
})
export class PackagesComponent implements OnInit, OnDestroy {
  packagesService: PackagesService = inject(PackagesService)
  private packageList!: IPackage[]
  unSubPackage!: Subscription
  breadCrumbs: BreadCrump[] = [
    { text: 'home', to: '/', active: true },
    { text: 'subscribe_as_presenter', to: '/subscription', active: true },
    { text: 'complete_payment', to: '/subscription', active: false },
  ]

  get items(): IPackage[] {
    return this.packageList
  }

  ngOnInit(): void {
    this.unSubPackage = this.packagesService.fetchPackageList$().subscribe(data => {
      this.packageList = data
    })
  }
  ngOnDestroy(): void {
    this.unSubPackage.unsubscribe()
  }

}
