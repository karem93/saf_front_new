export interface IPackage {
  id: number;
  name: string;
  description: string;
  duration: { type: string },
  price: number;
  price_after: number
  tax: number
  total: number
  [key: string]: any

}
