import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map } from "rxjs";
import { IPackage } from "./types";


@Injectable({ providedIn: 'root' })
export class PackagesService {
  constructor(private http: HttpClient) { }


  fetchPackageList$() {
    return this.http.get<{ data: IPackage[] }>('list_packages').pipe(map(res => res.data))
  }
}
