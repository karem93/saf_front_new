import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslationService } from 'assets/i18n/translation.service';
import { IPaymentData } from '../checkout/types';
import { map } from 'rxjs';
declare var Moyasar: any;

@Component({
  selector: 'app-payment',
  standalone: true,
  imports: [],
  templateUrl: './payment.component.html',
  styleUrl: './payment.component.scss',

})
export class PaymentComponent implements OnInit {
  paymentData!: IPaymentData
  subId!: string
  constructor(private transService: TranslationService, private http: HttpClient, private route: ActivatedRoute) {
    this.subId = this.route.snapshot.params['subId']

  }

  initPaymentForm() {
    Moyasar.init({
      element: '.mysr-form',
      amount: Number(this.paymentData.total_amount * 100),
      currency: localStorage.getItem('currency') || 'SAR',
      language: this.transService.currentLang,
      description: 'package order',
      publishable_api_key: 'pk_live_WhWyCgtENghBDEkApydXr6eJMn98LKJjBJyBuofY',
      callback_url: `${location.origin}/subscription/checkout-status?subId=${this.subId}`,

      // manual: true,
      save_card: "true",
      // on_redirect: function (url: any) {
      //   console.log(url, 'url');
      // },
      on_completed: function (payment: any) {
        return new Promise(function (resolve, reject) {
          if (payment) {
            resolve(payment);
          } else {
            reject();
          }
        });
      },
      methods: ['creditcard'],
      // on_completed: (payment: any) => {
      //   let self = this as any
      //   window.open(payment.source.transaction_url)
      //   return new Promise(function (resolve, reject) {
      //     if (self.onPaymentComplete(payment)) {
      //       resolve(payment);
      //     } else {
      //       reject();
      //     }
      //   });
      // },
    })
  }
  onPaymentComplete() {
    this.http.post(`update_subscription_status/${this.subId}`, {})
  }
  fetchPaymentData() {
    this.http.get<{ data: IPaymentData }>(`show_subscription/${this.subId}`).pipe(map(res => res.data)).subscribe(data => {
      this.paymentData = data
      this.initPaymentForm()
    })
  }
  ngOnInit(): void {

    this.fetchPaymentData()
  }
}
