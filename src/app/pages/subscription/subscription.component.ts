import { AfterViewInit, ChangeDetectorRef, Component, ComponentRef, ViewChild, ViewContainerRef } from '@angular/core';
import { PageTitleComponent } from 'app/components/shared/page-title/page-title.component';
import { BreadCrump } from '../../components/shared/page-title/model';
import { IStep } from './model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { slideAnimation } from 'app/animations/slide';
import { emailPattern, phoneValidation } from 'app/utils/patterns';
import { oneOfValidator } from 'app/validation/oneOfValidator';
import { FirstStepComponent } from 'app/components/subscription/first-step/first-step.component';
import { SecondStepComponent } from 'app/components/subscription/second-step/second-step.component';
import { emailValidator } from 'app/validation/emailValidator';
import { phoneValidator } from 'app/validation/phoneValidator';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { catchError, map, throwError } from 'rxjs';

@Component({
  selector: 'app-subscription',
  standalone: true,
  imports: [PageTitleComponent],
  templateUrl: './subscription.component.html',
  styleUrl: './subscription.component.scss',
  animations: [
    slideAnimation
  ],
})
export class SubscriptionComponent implements AfterViewInit {
  breadCrumb: BreadCrump[] = [
    { text: 'home', to: '/', active: true },
    { text: 'subscription', to: '/subscription', active: false }
  ]
  components: IStep[] = []
  active_index: number = 0
  form!: FormGroup;
  activeComponent!: IStep
  componentRef!: ComponentRef<any>
  @ViewChild('container', { read: ViewContainerRef }) container!: ViewContainerRef
  constructor(private fb: FormBuilder, private cdr: ChangeDetectorRef, private http: HttpClient, private router: Router) {
    this.form = this.fb.group({
      f_name: ['', [Validators.required, Validators.minLength(2)]],
      l_name: ['', [Validators.required, Validators.minLength(2)]],
      phone: ['', [Validators.required]],
      email: ['', [Validators.required, emailValidator()]],
      country: [null, [Validators.required]],
      sections: [null, [Validators.required]],

      type: ['visitor', [oneOfValidator(['visitor', 'presenter'])]],
    })


    this.components = [
      { component: FirstStepComponent, form: this.form },
      { component: SecondStepComponent, form: this.form },
    ]

  }


  loadComponent() {
    this.container.clear()
    this.activeComponent = this.components[this.active_index]
    this.componentRef = this.container.createComponent(this.activeComponent.component)
    this.componentRef.instance.form = this.activeComponent.form
    this.componentRef.instance.onNextStep?.subscribe(() => {
      this.nextStep()
    })
    this.componentRef.instance.onSubmit?.subscribe(() => {
      this.onSubmit()

    })
    this.cdr.detectChanges()
  }

  nextStep() {
    this.active_index++
    this.loadComponent()
  }
  generateData() {
    const formData: FormData = new FormData()
    const { country, sections, ...data }: { [key: string]: any } = this.form.value;
    for (const key in data) {
      formData.append(key, data[key]);
    }
    formData.append('phone_code', country.phone_code)
    formData.append('country_id', country.id);
    (sections as number[]).map((id: number, index: number) => {
      formData.append(`sections[${index}]`, id as any);
    })
    return formData
  }
  onSubmit() {

    this.http.post<{ data: { id: number } }>('addUser', this.generateData()).pipe(catchError((err: HttpErrorResponse) => {
      if (err) {
        this.active_index = 0
        this.loadComponent()
        this.componentRef.instance.loading = false

      }
      return throwError(() => err);
    }),
      map((res) => {
        const { data } = res

        if (this.form.value.type === 'presenter') {
          this.router.navigate([`subscription/complete/${data.id}`], {
            queryParams: {
              country_id: this.form.value.country?.id,
              sections: this.form.value.sections
            }
          });
        } else {
          this.router.navigate(['/'])
        }
        this.componentRef.instance.loading = false
        return res
      })

    ).subscribe()
  };
  ngAfterViewInit(): void {
    this.loadComponent()
  }
}

