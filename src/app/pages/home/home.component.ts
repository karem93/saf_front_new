import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { HomeService } from '../../store/home.service';
import { CommonModule } from '@angular/common';
import { StoresComponent } from '../../components/home/stores/stores.component';
import { AboutUsComponent } from '../../components/home/about-us/about-us.component';
import { ClientReviewsComponent } from '../../components/home/client-reviews/client-reviews.component';
import { SliderComponent } from 'app/components/shared/slider/slider.component';
import { StoreType } from 'app/store/types';
import { LazyLoadingContentComponent } from 'app/components/shared/lazy-loading-content/lazy-loading-content.component';
import { ShimmerLoadingDirective } from 'app/directives/shimmer-loading.directive';
import { BreakPoint } from 'app/components/shared/slider/model';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [TranslateModule, FormsModule, SliderComponent, CommonModule, StoresComponent, AboutUsComponent, ClientReviewsComponent, LazyLoadingContentComponent, ShimmerLoadingDirective],
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})
export class HomeComponent implements OnInit {
  storeItems: StoreType[] = []
  loading: boolean = true
  breakPoints: BreakPoint = {
    320: {
      slidesPerView: 1,
      spaceBetween: 10,
    }

  }
  constructor(public homeService: HomeService) {
  }


  ngOnInit(): void {
    this.homeService.homeData$.subscribe(data => {
      this.storeItems = data.stores.slice(0, 5)
      this.loading = false
    })
  }
}
