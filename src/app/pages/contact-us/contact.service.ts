import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { IForm } from "./types";

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(private http: HttpClient) { }


  handleSendMessage(data: unknown) {
    return this.http.post('contactUs', data)
  }
}
