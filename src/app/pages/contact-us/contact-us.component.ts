import { Component } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { ContactLink } from 'app/components/footer/model';
import { ButtonComponent } from 'app/components/shared/button/button.component';
import { FormGroupComponent } from 'app/components/shared/form-group/form-group.component';
import { PageTitleComponent } from 'app/components/shared/page-title/page-title.component';
import { InputFieldDirective } from 'app/directives/input-field.directive';
import { HomeService } from 'app/store/home.service';
import { ContactInfo } from 'app/store/types';
import { IForm } from './types';
import { emailValidator } from 'app/validation/emailValidator';
import { SelectComponent } from 'app/components/shared/select/select.component';
import { TranslationService } from 'assets/i18n/translation.service';
import { ContactService } from './contact.service';
import { catchError, of, tap } from 'rxjs';
import { GoogleMapComponent } from 'app/components/shared/google-map/google-map.component';

@Component({
  selector: 'app-contact-us',
  standalone: true,
  imports: [PageTitleComponent, FormGroupComponent, InputFieldDirective, ButtonComponent, TranslateModule, ReactiveFormsModule, SelectComponent, GoogleMapComponent],
  templateUrl: './contact-us.component.html',
  styleUrl: './contact-us.component.scss'
})
export class ContactUsComponent {
  contactLinks: ContactLink[] = [
    {
      text: 'contact_phone',
      key: "phone",
      href: 'tel:',
      icon: 'fa-solid fa-phone'
    },
    {
      text: 'email',
      key: "contact_email",
      href: 'mailto:',
      icon: 'fa-solid fa-envelope'
    }
  ]
  items: { name: string; value: string }[] = []
  loading: boolean = false
  form!: FormGroup<IForm>
  constructor(private homeService: HomeService, private fb: FormBuilder, private transService: TranslationService, private contactService: ContactService) {
    this.initializeForm()
    this.initialItems()
  }

  initialItems() {
    this.items = [
      { name: this.transService.getTransValue('suggestion'), value: "suggestion" },
      { name: this.transService.getTransValue('inquiry'), value: "inquiry" },
      { name: this.transService.getTransValue('complaint'), value: "complaint" },
    ];
  }

  initializeForm() {
    this.form = this.fb.group<IForm>({
      full_name: this.fb.control('', [Validators.required]),
      content: this.fb.control('', [Validators.required, Validators.minLength(20)]),
      email: this.fb.control('', [Validators.required, emailValidator()]),
      message_type: this.fb.control('', [Validators.required]),

    })
  }
  handleSubmit() {
    if (this.form.valid) {
      this.loading = true
      this.contactService.handleSendMessage(this.form.value).pipe(
        catchError(err => {
          if (err) {
            this.loading = false
          }
          return of(err)
        }),
        tap(res => {
          if (res.status) {
            this.loading = false
            this.form.reset()
          }
        })
      ).subscribe()

    } else {
      this.form.markAllAsTouched()
    }
  }
  get contactInfo() {
    let data!: ContactInfo;
    this.homeService.contactInfo$.subscribe(res => {
      data = res
    })
    return data
  }
}
