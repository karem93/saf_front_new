import { FormControl } from "@angular/forms";

export interface IForm {
  full_name: FormControl<string | null>
  message_type: FormControl<string | null>;
  email: FormControl<string | null>;
  content: FormControl<string | null>
  [key: string]: any

}
