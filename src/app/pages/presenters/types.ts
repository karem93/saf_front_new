import { Country } from "app/store/types";

export interface IPresenter {
  id: number;
  name: string
  company_img: string;
  company_name: string;
  country: Country
  bio: string;
  email: string;
  facebook: string;
  instagram: string;
  whatsAPP: number;
  phone: number;
  twitter: string;
  [key: string]: any
}


export type TQuery = {
  search: string;
  sort: string
}

