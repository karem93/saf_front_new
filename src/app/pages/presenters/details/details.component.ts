import { Component, OnDestroy, OnInit, inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { ContactLink } from 'app/components/footer/model';
import { BreadCrump } from 'app/components/shared/page-title/model';
import { PageTitleComponent } from 'app/components/shared/page-title/page-title.component';
import { SocialType } from 'app/components/top-bar/model';
import { Product, Slider } from 'app/store/types';
import { PresenterDetailsService } from './details.service';
import { IPresenter } from '../types';
import { SectionTitleComponent } from 'app/components/shared/section-title/section-title.component';
import { Subscription } from 'rxjs';
import { SliderComponent } from 'app/components/shared/slider/slider.component';
import { ProductCardComponent } from 'app/components/product-card/product-card.component';
import { PresenterCardComponent } from 'app/components/presenter-card/presenter-card.component';

@Component({
  selector: 'app-details',
  standalone: true,
  imports: [PageTitleComponent, TranslateModule, SectionTitleComponent, SliderComponent, ProductCardComponent, PresenterCardComponent],
  templateUrl: './details.component.html',
  styleUrl: './details.component.scss'
})
export class DetailsComponent implements OnInit, OnDestroy {
  route: ActivatedRoute = inject(ActivatedRoute)
  presenterDetailsService: PresenterDetailsService = inject(PresenterDetailsService)

  presenter: IPresenter = {
    id: 0,
    name: '',
    company_img: '',
    company_name: "",
    country: {
      id: 0,
      name: '',
      flag: '',
      phone_code: ''
    },
    bio: '',
    email: '',
    facebook: '',
    instagram: '',
    whatsAPP: 0,
    phone: 0,
    twitter: ''
  }
  products: Product[] = []
  sliders: Slider[] = []
  unSubscription!: Subscription
  breadCrumb: BreadCrump[] = [
    { text: "home", active: true, to: '/' },
    { text: "presenters", active: true, to: `/presenters/${this.route.snapshot.params['id']}` },
    { text: "presenter_details", active: false, to: '/' },
  ]
  contactLinks: ContactLink[] = [
    {
      text: 'contact_phone',
      key: "phone",
      href: 'tel:',
      icon: 'fa-solid fa-phone'
    },
    {
      text: 'email',
      key: "email",
      href: 'mailto:',
      icon: 'fa-solid fa-envelope'
    }
  ]
  socials: SocialType[] = [
    { icon: 'fa-brands fa-instagram', value: 'instagram' },
    { icon: 'fa-brands fa-twitter', value: 'twitter' },
    { icon: 'fa-brands fa-facebook-f', value: 'facebook' },
  ]


  ngOnInit(): void {
    const id = this.route.snapshot.params['id']
    const storeId = this.route.snapshot.params['storeId']
    this.unSubscription = this.presenterDetailsService.getPresenterData(id, storeId).subscribe(data => {
      this.presenter = data.presenter
      this.products = data.products
      this.sliders = data.sliders
    })
  }
  ngOnDestroy(): void {
    if (this.unSubscription) {
      this.unSubscription.unsubscribe()
    }

  }
}
