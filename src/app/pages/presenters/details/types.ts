import { IPresenter } from 'app/pages/presenters/types';
import { Product, Slider } from '../../../store/types';
export interface IPresenterData {
  presenter: IPresenter; products: Product[]; sliders: Slider[]
}
