import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map } from "rxjs";

import { IPresenterData } from "./types";

@Injectable({
  providedIn: 'root'
})
export class PresenterDetailsService {

  constructor(private http: HttpClient) { }

  getPresenterData(id: string, storeId: string) {
    return this.http.get<{ data: IPresenterData }>(`presenters/${id}/${storeId}`).pipe(
      map(res => res.data)
    )
  }
}
