import { Component, OnInit } from '@angular/core';
import { PageTitleComponent } from 'app/components/shared/page-title/page-title.component';
import { IPresenter, TQuery } from './types';
import { PresentersService } from './presenters.service';
import { MetaOption } from '../sections/types';
import { PresenterCardComponent } from 'app/components/presenter-card/presenter-card.component';
import { SearchFilterComponent } from 'app/components/shared/search-filter/search-filter.component';
import { TranslateModule } from '@ngx-translate/core';
import { ButtonComponent } from 'app/components/shared/button/button.component';
import { SelectFilterComponent } from 'app/components/shared/select-filter/select-filter.component';
import { IOption } from 'app/components/shared/select-filter/types';
import { FormsModule, } from '@angular/forms';
import { LoaderComponent } from 'app/components/shared/loader/loader.component';
import { BreadCrump } from 'app/components/shared/page-title/model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-presenters',
  standalone: true,
  imports: [PageTitleComponent, PresenterCardComponent, SearchFilterComponent, TranslateModule, ButtonComponent, SelectFilterComponent, FormsModule, LoaderComponent],
  templateUrl: './presenters.component.html',
  styleUrl: './presenters.component.scss'
})
export class PresentersComponent implements OnInit {
  presenterId: string = ''
  presenters: IPresenter[] = []
  metaOption: MetaOption = {
    current_page: 0,
    last_page: 0
  }
  loading: boolean = true
  loadingMore: boolean = false
  breadCrumb: BreadCrump[] = [
    { text: "home", active: true, to: '/' },
    { text: "presenters", active: false, to: `/` },
  ]
  options: IOption[] = [
    { text: 'most_views', value: 'most_views' },
    { text: 'latest', value: 'latest' },
  ]
  queryString: TQuery = {
    sort: 'most_views',
    search: ''
  }
  constructor(private presenterService: PresentersService, private route: ActivatedRoute) {
    this.presenterId = this.route.snapshot.params['id']
  }


  ngOnInit(): void {
    this.getPresenters()
  }

  getPresenters(query = {}) {
    this.loading = true
    this.presenterService.fetchPresenters(query, this.presenterId).subscribe(res => {
      const { data, meta } = res
      if (meta.current_page > 1) {
        this.presenters = [...this.presenters, ...data]
      } else {

        this.presenters = data
      }
      this.metaOption = meta
      this.loading = false
    })

  }
  handleFilter(item: IOption) {
    this.queryString.sort = item.value
  }
  onFilter() {
    this.presenters = []
    this.getPresenters(this.queryString)
  }
  loadMore() {
    this.getPresenters({ ...this.queryString, page: this.metaOption.current_page + 1 })
  }
  get storeName(): string {
    return this.route.snapshot.params['storeName']
  }
}
