import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { IPresenter } from "./types";
import { map } from "rxjs";
import { MetaOption } from "../sections/types";

@Injectable({ providedIn: 'root' })
export class PresentersService {
  constructor(private http: HttpClient, private route: ActivatedRoute) { }

  fetchPresenters(query: { [key: string]: any } = {}, id: string) {

    return this.http.get<{ data: IPresenter[], meta: MetaOption }>(`presenters/${id}`, { params: { ...query } })
  }
}
