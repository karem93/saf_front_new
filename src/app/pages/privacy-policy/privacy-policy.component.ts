import { Component } from '@angular/core';
import { StaticPageComponent } from 'app/components/static-page/static-page.component';

@Component({
  selector: 'app-privacy-policy',
  standalone: true,
  imports: [StaticPageComponent],
  templateUrl: './privacy-policy.component.html',
  styleUrl: './privacy-policy.component.scss'
})
export class PrivacyPolicyComponent {

}
