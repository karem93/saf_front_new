import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { IQuestion } from "./type";
import { map } from "rxjs";

@Injectable({ providedIn: 'root' })
export class CommonQuestionService {

  constructor(private http: HttpClient) { }



  fetchQuestions() {
    return this.http.get<{ data: IQuestion[] }>('faqs').pipe(map(res => res.data))
  }
}
