import { Component } from '@angular/core';
import { CollapseComponent } from 'app/components/shared/collapse/collapse.component';
import { PageTitleComponent } from 'app/components/shared/page-title/page-title.component';
import { Observable, } from 'rxjs';
import { IQuestion } from './type';
import { CommonQuestionService } from './common-questions.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-common-questions',
  standalone: true,
  imports: [PageTitleComponent, CollapseComponent, CommonModule],
  templateUrl: './common-questions.component.html',
  styleUrl: './common-questions.component.scss'
})
export class CommonQuestionsComponent {
  questions$!: Observable<IQuestion[]>
  loading: boolean = true
  constructor(private commonQuestionService: CommonQuestionService) {
    this.questions$ = this.commonQuestionService.fetchQuestions()
  }


}
