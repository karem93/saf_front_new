import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { StoreType } from "app/store/types";
import { MetaOption } from "../sections/types";

@Injectable({ providedIn: 'root' })
export class LatestStoreService {
  constructor(private http: HttpClient) { }


  fetchLatestStore(page: number = 1) {
    return this.http.get<{ data: StoreType[], meta: MetaOption }>('loadStores', { params: { page } })
  }

}
