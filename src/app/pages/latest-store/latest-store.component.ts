import { Component, OnInit } from '@angular/core';
import { StoresComponent } from 'app/components/home/stores/stores.component';
import { PageTitleComponent } from 'app/components/shared/page-title/page-title.component';
import { StoreType } from 'app/store/types';
import { MetaOption } from '../sections/types';
import { LatestStoreService } from './latest-store.service';
import { LoaderComponent } from 'app/components/shared/loader/loader.component';
import { exhaustMap, of, tap } from 'rxjs';
import { ButtonComponent } from 'app/components/shared/button/button.component';
import { TranslateModule } from '@ngx-translate/core';

@Component({
  selector: 'app-latest-store',
  standalone: true,
  imports: [PageTitleComponent, StoresComponent, LoaderComponent, ButtonComponent, TranslateModule],
  templateUrl: './latest-store.component.html',
  styleUrl: './latest-store.component.scss'
})
export class LatestStoreComponent implements OnInit {
  stores: StoreType[] = []
  metaOption!: MetaOption
  loading: boolean = true
  constructor(private latestService: LatestStoreService) {
  }

  ngOnInit(): void {
    this.getStores()
  }

  getStores(page: number = 1) {
    this.loading = true
    this.latestService.fetchLatestStore(page)
      .pipe(
        exhaustMap((res) => {
          const { data, meta } = res
          this.stores = [...this.stores, ...data]
          this.metaOption = meta
          return of(res)
        }),
        tap((e) => {
          if (e.data) {
            this.loading = false
          }

        })
      )
      .subscribe()
  }

  loadMore() {
    this.getStores(this.metaOption.current_page + 1)
  }

}
