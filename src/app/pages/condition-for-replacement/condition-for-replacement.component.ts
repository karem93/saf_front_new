import { Component } from '@angular/core';
import { StaticPageComponent } from 'app/components/static-page/static-page.component';

@Component({
  selector: 'app-condition-for-replacement',
  standalone: true,
  imports: [StaticPageComponent],
  templateUrl: './condition-for-replacement.component.html',
  styleUrl: './condition-for-replacement.component.scss'
})
export class ConditionForReplacementComponent {

}
