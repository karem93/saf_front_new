export const phoneValidation = /^(009665|9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/
export const emailPattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
