export const navLinks: { href: string; text: string }[] = [
  {
    text: 'home',
    href: '/'
  },
  { text: "sections", href: '/sections' },
  { text: "common_questions", href: '/common-questions' },
  { text: "contact_us", href: '/contact-us' },
]
