import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationEnd, Router, RouterOutlet } from '@angular/router';
import { TranslationService } from '../assets/i18n/translation.service';
import { TopBarComponent } from './components/top-bar/top-bar.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { fadeAnimation } from './animations/fade';
import { FooterComponent } from './components/footer/footer.component';
import { FloatingButtonComponent } from './components/floating-button/floating-button.component';
import { SnackBarComponent } from './components/snack-bar/snack-bar.component';
import { filter } from 'rxjs';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, TopBarComponent, NavBarComponent, FooterComponent, FloatingButtonComponent, SnackBarComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
  animations: [
    fadeAnimation
  ],
})
export class AppComponent implements OnInit {
  title = 'saf-project';
  constructor(private transService: TranslationService, private router: Router) { }

  ngOnInit() {
    this.transService.init()
    this.scrollToTop()
  }

  scrollToTop() {
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(() => {
        window.scrollTo({ top: 0, behavior: 'smooth' });
      });
  }

}
