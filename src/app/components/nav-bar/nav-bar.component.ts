import { Component, ViewChild, ViewContainerRef } from '@angular/core';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { ButtonComponent } from '../shared/button/button.component';
import { navLinks } from "../../utils/navLinks"
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { SideBarComponent } from '../side-bar/side-bar.component';
@Component({
  selector: 'nav-bar',
  standalone: true,
  imports: [CommonModule, TranslateModule, RouterLink, RouterLinkActive, ButtonComponent, SideBarComponent],
  templateUrl: './nav-bar.component.html',
  styleUrl: './nav-bar.component.scss'
})
export class NavBarComponent {
  navLinks = navLinks

  @ViewChild(SideBarComponent) sidebar!: SideBarComponent

  onToggle() {
    this.sidebar.toggleShow()
  }
}
