import { Component, Input, OnInit } from '@angular/core';
import { ButtonComponent } from '../shared/button/button.component';
import { TranslateModule } from '@ngx-translate/core';
import { IPresenter } from 'app/pages/presenters/types';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslationService } from 'assets/i18n/translation.service';

@Component({
  selector: 'presenter-card',
  standalone: true,
  imports: [ButtonComponent, TranslateModule],
  templateUrl: './presenter-card.component.html',
  styleUrl: './presenter-card.component.scss'
})
export class PresenterCardComponent implements OnInit {
  @Input() item!: IPresenter
  @Input() navigation: boolean = true
  id!: string;

  constructor(private route: ActivatedRoute, private router: Router, private transService: TranslationService) { }

  ngOnInit(): void {
    this.setId()
  }
  redirect() {
    this.router.navigate(['presenters', this.id, 'details', this.item.id])
  }

  setId() {
    this.id = this.route.snapshot.params['id']
  }
  get lang() {
    return this.transService.lang
  }
}
