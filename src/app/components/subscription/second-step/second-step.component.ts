import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, OnDestroy, OnInit, inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ButtonComponent } from 'app/components/shared/button/button.component';
import { TabsComponent } from 'app/components/shared/tabs/tabs.component';
import { Tab } from 'app/components/shared/tabs/types';
import { ShimmerLoadingDirective } from 'app/directives/shimmer-loading.directive';
import { TranslationService } from 'assets/i18n/translation.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-second-step',
  standalone: true,
  imports: [TabsComponent, TranslateModule, ButtonComponent, ShimmerLoadingDirective],
  templateUrl: './second-step.component.html',
  styleUrl: './second-step.component.scss'
})
export class SecondStepComponent implements OnInit, OnDestroy {
  @Input() form!: FormGroup;
  loading: boolean = false
  fetching: boolean = true
  onSubmit: EventEmitter<boolean> = new EventEmitter()
  textContent: { content: string } = { content: '' }
  http: HttpClient = inject(HttpClient)
  unSub!: Subscription
  transService: TranslationService = inject(TranslationService)
  tabs: Tab[] = [
    { text: 'visitor', value: 'visitor' },
    { text: 'presenter', value: 'presenter' },
  ]

  onTab(value: any) {
    this.form.patchValue({ type: value })
    this.fetchIntroductoryText()
  }

  handleSubmit() {
    this.loading = true
    this.onSubmit.emit()
  }
  get value(): string {
    return this.form.value.type
  }

  fetchIntroductoryText() {

    this.unSub = this.http.get<{ data: { content: string } }>('introductoryText', { params: { type: this.form?.value.type } }).subscribe(res => {
      this.textContent = res.data
      this.fetching = false
    })
  }
  ngOnInit(): void {
    this.fetchIntroductoryText()
  }
  ngOnDestroy(): void {
    if (this.unSub) {
      this.unSub.unsubscribe()
    }
  }

  get Img(): string {
    return `/assets/images/${this.transService.currentLang}_bg.svg`
  }
}
