import { Component, EventEmitter, Input } from '@angular/core';
import { FormGroup, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ButtonComponent } from 'app/components/shared/button/button.component';
import { FormGroupComponent } from 'app/components/shared/form-group/form-group.component';
import { SelectComponent } from 'app/components/shared/select/select.component';
import { InputFieldDirective } from 'app/directives/input-field.directive';
import { FirstStepService } from './first-step.service';
import { Country, Section } from './type';

@Component({
  selector: 'app-first-step',
  standalone: true,
  imports: [TranslateModule, FormGroupComponent, InputFieldDirective, ReactiveFormsModule, ButtonComponent, SelectComponent],
  templateUrl: './first-step.component.html',
  styleUrl: './first-step.component.scss'
})
export class FirstStepComponent {
  @Input() form!: FormGroup
  onNextStep: EventEmitter<() => void> = new EventEmitter()
  countries: Country[] = []
  sections: Section[] = []
  loadingCountries = true;
  loadingSections = true;
  constructor(private firstStepService: FirstStepService) {
    this.firstStepService.fetchCountries().subscribe(res => {
      this.countries = res.data
      this.loadingCountries = false
    })
    this.firstStepService.fetchSections().subscribe(res => {
      this.sections = res.data
      this.loadingSections = false
    })
  }
  handleNext() {
    if (this.form.valid) {
      // in case form is valid we will go to the next step
      this.onNextStep.emit()
    } else {
      this.form.markAllAsTouched()
    }
  }
}
