import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Country, Section } from './type';

@Injectable({
  providedIn: 'root'
})
export class FirstStepService {

  constructor(private http: HttpClient) { }


  fetchCountries() {
    return this.http.get<{ data: Country[] }>('list_countries')
  }
  fetchSections() {
    return this.http.get<{ data: Section[] }>('list_sections')
  }
}
