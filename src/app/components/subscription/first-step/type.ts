export interface Country {
  phone_code: string;
  flag: string;
  name: string;
  id: string
}


export type Section = Omit<Country, 'phone_code'>
