import { Component, Input } from '@angular/core';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IPackage } from 'app/pages/subscription/packages/types';

@Component({
  selector: 'package-card',
  standalone: true,
  imports: [TranslateModule, RouterLink],
  templateUrl: './package-card.component.html',
  styleUrl: './package-card.component.scss'
})
export class PackageCardComponent {
  @Input() item!: IPackage
  userId: string = ''
  store_id: string = ''
  constructor(private route: ActivatedRoute) {
    this.userId = this.route.snapshot.params['userId']
    this.store_id = this.route.snapshot.queryParams['store_id']
  }
}
