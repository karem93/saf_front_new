import { Component } from '@angular/core';
import { navLinks } from '../../utils/navLinks';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

@Component({
  selector: 'side-bar',
  standalone: true,
  imports: [RouterLink, RouterLinkActive, CommonModule, TranslateModule],
  templateUrl: './side-bar.component.html',
  styleUrl: './side-bar.component.scss'
})
export class SideBarComponent {
  show: boolean = false;
  navLinks = navLinks
  toggleShow() {
    this.show = !this.show;
  }
}
