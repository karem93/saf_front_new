import { CommonModule } from '@angular/common';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { HomeService } from 'app/store/home.service';
import { ContactLink, LinkType } from './model';
import { InputDirective } from 'app/directives/input.directive';
import { ButtonComponent } from '../shared/button/button.component';
import { HttpClient } from '@angular/common/http';
import { GoogleMapComponent } from '../shared/google-map/google-map.component';
import { ContactInfo } from 'app/store/types';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-footer',
  standalone: true,
  imports: [TranslateModule, CommonModule, InputDirective, ButtonComponent, GoogleMapComponent, RouterLink],
  templateUrl: './footer.component.html',
  styleUrl: './footer.component.scss',
})
export class FooterComponent {
  parseInt(arg: number): number {
    return Number(arg)
  }
  @ViewChild('input') inputField!: ElementRef<HTMLInputElement>
  loading: boolean = false;
  important_links: LinkType[] = [
    {
      text: 'subscribe',
      href: '/subscription',
    },
    {
      text: 'condition_for_replacement',
      href: '/condition-for-replacement',
    },
    {
      text: 'contact_us',
      href: '/contact-us',
    },
    {
      text: 'privacy_policy',
      href: '/privacy-policy',
    },
    {
      text: 'about_us',
      href: '/about-us',
    },
    {
      text: 'common_questions',
      href: '/common-questions',
    }
  ]

  contactLinks: ContactLink[] = [
    {
      text: 'address',
      href: '',
      icon: 'fa-solid fa-location-dot'
    },
    {
      text: 'phone',
      href: 'tel:',
      icon: 'fa-solid fa-phone'
    },
    {
      text: 'contact_email',
      href: 'mailto:',
      icon: 'fa-solid fa-envelope'
    }
  ]
  socialLinks: ContactLink[] = [
    {
      text: 'facebook_link',
      href: '',
      icon: 'fa-brands fa-facebook-f'
    },
    {
      text: 'twitter_link',
      href: 'tel:',
      icon: 'fa-brands fa-twitter'
    },
    {
      text: 'instagram_link',
      href: '',
      icon: 'fa-brands fa-instagram'
    },
    {
      text: 'youtube_link',
      href: '',
      icon: 'fa-brands fa-youtube'
    },
    {
      text: 'snapchat_link',
      href: '',
      icon: 'fa-brands fa-snapchat'
    }
  ]
  constructor(public homeService: HomeService, private http: HttpClient) { }




  sendEmail() {
    if (this.inputField.nativeElement.value) {
      this.loading = true
      this.http.post('newsletter', { email: this.inputField.nativeElement.value }).subscribe(() => {
        this.inputField.nativeElement.value = ''
        this.loading = false
      })
    }
  }

}
