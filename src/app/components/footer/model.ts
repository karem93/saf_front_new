export interface LinkType {
  text: string;
  href: string
}

export interface ContactLink extends LinkType {
  icon: string;
  key?: string
}
