import { AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, ContentChild, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, TemplateRef, ViewChild } from '@angular/core';
import { ButtonComponent } from '../button/button.component';
import { TranslationService } from '../../../../assets/i18n/translation.service';
import { CommonModule, NgTemplateOutlet } from '@angular/common';
import { BreakPoint, SlideStyle } from './model';
import Hammer from 'hammerjs';

@Component({
  selector: 'app-slider',
  standalone: true,
  imports: [ButtonComponent, CommonModule, NgTemplateOutlet],
  templateUrl: './slider.component.html',
  styleUrl: './slider.component.scss'
})
export class SliderComponent<S> implements AfterViewChecked, OnChanges, AfterViewInit {
  cardWidth: number = 0
  activeIndex: number = 0
  slideScrollLeft: number = 0
  visiblePerSlide: number = 1
  spaceBetween: number = 45
  slideStyle: SlideStyle = { 'grid-auto-columns': `calc((100% / ${this.visiblePerSlide}) - 16px)` }
  @Input() slides: S[] = []
  @Input() navigation: boolean = true
  @Input() pagination: boolean = true
  @Input() breakPoints: BreakPoint = {}
  @ViewChild('card') card: ElementRef<HTMLElement> | undefined
  @ViewChild('slide') slide!: ElementRef<HTMLElement>
  @ContentChild('swiperContent') swiperContent!: TemplateRef<any>;
  @Output() onSlide: EventEmitter<number> = new EventEmitter();
  constructor(private tranService: TranslationService, private cdr: ChangeDetectorRef) { }
  handleNext() {
    const slidesLeft = this.slides?.length - (this.activeIndex + this.visiblePerSlide);
    const slidesToMove = Math.min(this.visiblePerSlide, slidesLeft);
    this.activeIndex += slidesToMove;
    this.scrollToActiveIndex();
  }

  handlePrev() {
    const slidesToMove = Math.min(this.visiblePerSlide, this.activeIndex);
    this.activeIndex -= slidesToMove;
    this.scrollToActiveIndex();
  }
  goToSlide(index: number) {
    this.activeIndex = index * this.visiblePerSlide;
    this.scrollToActiveIndex();
  }
  scrollToActiveIndex() {
    const slidesToMove = this.cardWidth * this.scrollDirection * this.activeIndex
    this.slide.nativeElement.scrollTo({ left: slidesToMove, behavior: 'smooth' })
    this.onSlide.emit(this.activeIndex)

  }



  changeSlide(distance: number) {
    // Determine the direction of the drag
    if (distance > 0) {
      // Dragging to the right
      this.handleNext()
    } else if (distance < 0) {
      // Dragging to the left
      this.handlePrev()
    }
  }
  get scrollDirection(): number {
    return this.tranService.direction === 'rtl' ? -1 : 1;
  }
  get displayedIndicators() {
    const totalSlides = this.slides?.length || 0;
    const slidesPerGroup = this.visiblePerSlide;
    const totalGroups = Math.ceil(totalSlides / slidesPerGroup);
    const activeGroup = Math.floor(this.activeIndex / slidesPerGroup);

    return Array.from({ length: totalGroups }, (_, i) => i === activeGroup ? 'active' : '');
  }


  get customStyle(): SlideStyle {
    return { 'grid-auto-columns': `calc((100% / ${this.visiblePerSlide}))`, gap: `${this.spaceBetween}px` }
  }

  // life cycle
  ngAfterViewChecked(): void {
    if (this.card) {
      this.cardWidth = this.card.nativeElement.offsetWidth;
    }
    if (this.slides && this.slides.length > 0) {
      this.slideScrollLeft = this.slide.nativeElement.scrollLeft;
    }
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes['breakPoints']) {
      const currentValue = changes['breakPoints'].currentValue;
      const observer = new ResizeObserver((entries) => {
        for (const entry of entries) {
          for (const key in currentValue) {
            if (entry.contentRect.width >= Number(key)) {
              if (currentValue[key]) {
                this.visiblePerSlide = currentValue[key]['slidesPerView'];
                if (currentValue[key]['spaceBetween']) {
                  this.spaceBetween = currentValue[key]['spaceBetween']
                }
                this.cdr.detectChanges()
              }
            }
          }
        }
      });

      observer.observe(document.body);
    }
  }
  ngAfterViewInit() {
    const slide = this.slide.nativeElement;
    const hammer = new Hammer(slide);

    let initialX = 0;

    hammer.get('pan').set({ direction: Hammer.DIRECTION_HORIZONTAL });

    hammer.on('panstart', (event: any) => {
      initialX = slide.scrollLeft;
      event.preventDefault();
    });

    hammer.on('panend', (event: any) => {
      const delta = event.deltaX;
      this.changeSlide(delta)
    });
  }

}
