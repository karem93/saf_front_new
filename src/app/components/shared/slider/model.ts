export interface BreakPoint {
  [key: number]: {
    slidesPerView: number;
    spaceBetween?: number

  }
}


export interface SlideStyle {
  [key: string]: any
}
