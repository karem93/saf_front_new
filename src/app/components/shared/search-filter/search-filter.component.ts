import { Component } from '@angular/core';
import { InputFieldDirective } from 'app/directives/input-field.directive';

@Component({
  selector: 'search-filter',
  standalone: true,
  imports: [InputFieldDirective],
  templateUrl: './search-filter.component.html',
  styleUrl: './search-filter.component.scss'
})
export class SearchFilterComponent {

}
