import { CommonModule } from '@angular/common';
import { Component, Input, OnDestroy } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors } from '@angular/forms';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { errorMessageAnimate } from 'app/animations/errorMessage';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-form-group',
  templateUrl: './form-group.component.html',
  styleUrls: ['./form-group.component.scss'],
  standalone: true,
  imports: [CommonModule, TranslateModule],
  animations: [
    errorMessageAnimate
  ],
})
export class FormGroupComponent implements OnDestroy {
  @Input() control!: AbstractControl | any;
  private unSub: Subscription = new Subscription();

  constructor(private transService: TranslateService) {
    this.transService.get('home').subscribe();
  }

  // get validationMessage(): string {
  //   if (this.control && this.control.invalid && this.control.touched) {
  //     const errors = this.control.errors || {};
  //     const errorKey = Object.keys(errors)[0];
  //     return this.getErrorMessage(errorKey, errors[errorKey]);
  //   }
  //   return '';
  // }

  private getErrorMessage(errorType: string, errorValue: any): string {
    let errorMessage = '';
    this.unSub = this.transService.get(`validations.${errorType}`, { ...errorValue }).subscribe((message) => {
      errorMessage = message;
    });
    return errorMessage;
  }
  get inValid(): boolean {
    return this.checkValidity(this.control);
  }

  private checkValidity(control: any): boolean {
    if (!control) {
      return false;
    }

    if (control instanceof FormControl) {
      return control.invalid && control.touched;
    }

    if (control instanceof FormGroup) {
      for (const key in control.controls) {
        if (control.controls.hasOwnProperty(key)) {
          const nestedControl = control.controls[key];
          if (this.checkValidity(nestedControl)) {
            return true;
          }
        }
      }
    }

    return false;
  }
  ngOnDestroy(): void {
    this.unSub.unsubscribe();
  }

  // Recursive method to handle nested FormGroup validation messages
  getNestedValidationMessage(control: AbstractControl): string {
    if (control instanceof FormGroup) {
      for (const key in control.controls) {
        if (control.controls.hasOwnProperty(key)) {
          const nestedControl = control.get(key);
          if (nestedControl) {
            const message = this.getNestedValidationMessage(nestedControl);
            if (message) {
              return message;
            }
          }
        }
      }
    } else if (control instanceof FormControl && control.invalid && control.touched) {
      const errors: ValidationErrors | null = control.errors;
      if (errors) {
        const errorKey = Object.keys(errors)[0];
        return this.getErrorMessage(errorKey, errors[errorKey]);
      }
    }
    return '';
  }
}
