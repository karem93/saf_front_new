import { Component, Input } from '@angular/core';
import { RouterLink } from '@angular/router';
import { BreadCrump } from './model';
import { TranslateModule } from '@ngx-translate/core';

@Component({
  selector: 'page-title',
  standalone: true,
  imports: [RouterLink, TranslateModule],
  templateUrl: './page-title.component.html',
  styleUrl: './page-title.component.scss'
})
export class PageTitleComponent {
  @Input() breadCurmb: BreadCrump[] = []
  @Input() pageHeading?: string = ''
}
