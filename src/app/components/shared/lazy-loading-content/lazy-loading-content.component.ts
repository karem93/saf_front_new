import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'lazy-loading-content',
  standalone: true,
  imports: [],
  templateUrl: './lazy-loading-content.component.html',
  styleUrl: './lazy-loading-content.component.scss'
})
export class LazyLoadingContentComponent implements AfterViewInit {
  isVisible: boolean = false;
  observer!: IntersectionObserver
  @ViewChild('lazyContent') lazyContent!: ElementRef<HTMLElement>;

  options = {
    root: null,
    rootMargin: '0px',
    threshold: 0,
  };


  handleIntersection = (entries: IntersectionObserverEntry[]) => {
    entries.forEach((entry) => {
      if (entry.isIntersecting) {
        this.isVisible = true;
      }
      // else {
      //   this.isVisible = false;
      // }
    });
  };

  ngAfterViewInit(): void {
    this.observer = new IntersectionObserver(this.handleIntersection, this.options);
    this.observer.observe(this.lazyContent.nativeElement);
  }
}
