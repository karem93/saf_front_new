import { Component, Input } from '@angular/core';
import { LoadingSpinnerComponent } from '../loading-spinner/loading-spinner.component';

@Component({
  selector: 'app-loader',
  standalone: true,
  imports: [LoadingSpinnerComponent],
  templateUrl: './loader.component.html',
  styleUrl: './loader.component.scss'
})
export class LoaderComponent<T> {
  @Input() loading: boolean = true;
  @Input() notFound: boolean = true;
  @Input() items: T[] = [];
}
