import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Tab } from './types';
import { TranslateModule } from '@ngx-translate/core';

@Component({
  selector: 'app-tabs',
  standalone: true,
  imports: [TranslateModule],
  templateUrl: './tabs.component.html',
  styleUrl: './tabs.component.scss'
})
export class TabsComponent {
  @Input() tabs: Tab[] = []
  @Input() value: string = ''
  @Output() onClick: EventEmitter<string> = new EventEmitter()
  onSelectTab(value: string) {
    this.onClick.emit(value)
  }
}
