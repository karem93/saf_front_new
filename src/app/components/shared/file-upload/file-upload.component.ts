import { CommonModule } from '@angular/common';
import { Component, ElementRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

@Component({
  selector: 'file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss'],
  standalone: true,
  imports: [CommonModule, TranslateModule],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: FileUploadComponent, multi: true }
  ]
})
export class FileUploadComponent implements ControlValueAccessor {

  @Input() accept: string = 'image/png';
  @Input() placeholder: string = '';
  imgUrl!: string;

  // Value accessor functions
  onChange: Function = () => { };
  onTouched: Function = () => { };

  onFileChange(event: Event) {
    const inputElement = event.target as HTMLInputElement;
    const files: FileList | null = inputElement.files;

    if (files) {
      const file: File = files[0];
      this.imgUrl = URL.createObjectURL(file);
      this.onChange(file); // Notify the value accessor that the value has changed
    }
    this.onTouched(); // Notify the value accessor that the input element has been touched
  }
  onRemoveFile(input: HTMLInputElement) {
    this.imgUrl = ''
    input.value = ''
    this.onChange('')
  }
  writeValue(file: File | string): void {
    if (file && typeof file == 'object') {
      this.imgUrl = URL.createObjectURL(file);
    } else {
      this.imgUrl = file

    }
    // You can update the component's state based on the value provided from the form control.
  }

  registerOnChange(onChange: Function): void {
    this.onChange = onChange;
    // This method is called by the form control to register a callback function for value changes.
  }

  registerOnTouched(onTouched: Function): void {
    this.onTouched = onTouched;
    // This method is called by the form control to register a callback function for touch events.
  }
}
