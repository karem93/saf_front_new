import { Component, ElementRef, HostListener, Input, ViewChild } from '@angular/core';
import { ControlValueAccessor, FormsModule, NG_VALUE_ACCESSOR } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { InputFieldDirective } from 'app/directives/input-field.directive';
import { FilterPipe } from 'app/pipes/filter.pipe';

@Component({
  selector: 'app-select',
  standalone: true,
  imports: [InputFieldDirective, TranslateModule, FilterPipe, FormsModule],
  templateUrl: './select.component.html',
  styleUrl: './select.component.scss',
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: SelectComponent, multi: true }
  ]
})
export class SelectComponent<T extends { [key: string]: any }> implements ControlValueAccessor {
  @Input() items: T[] = []
  @Input() itemKey: string = ''
  @Input() itemValue: string = ''
  @Input() placeholder: string = ''
  @Input() autocomplete: boolean = false
  @Input() loading: boolean = false
  @Input() returnObject: boolean = false
  @Input() multiple: boolean = false
  @ViewChild('menu') menu!: ElementRef<HTMLElement>

  showMenu: boolean = false;
  search: string = '';

  selectedItem: { [key: string]: any } | null = {}
  selectedListItems: any[] = []
  itemListByItemValue: any[] = []

  // Value accessor functions
  onChange: Function = () => { };
  onTouched: Function = () => { };
  constructor(private _element: ElementRef<HTMLElement>) { }
  writeValue(value: T extends [] ? [] : {}): void {
    if (this.multiple) {
      if (value && Array.isArray(value)) {
        this.itemListByItemValue = value
      }
    }
    if (this.returnObject) {
      if (value !== null && value !== undefined) {
        this.selectedItem = value;
        this.search = this.selectValue

      }

    }

  }
  registerOnChange(fn: Function): void {
    this.onChange = fn
  }
  registerOnTouched(fn: Function): void {
    this.onTouched = fn
  }


  onToggleMenu() {
    if (!this.loading && !this.autocomplete) {
      this.showMenu = !this.showMenu
    } else if (!this.loading && this.autocomplete) {
      this.showMenu = true
    } else {
      this.showMenu = !this.showMenu

    }
    if (this.menu.nativeElement.offsetTop > 0) {

      this.menu.nativeElement.scrollTo({ top: 0, behavior: 'smooth' })
    }

  }
  isInList(item: any): boolean {
    return this.selectedListItems.includes(item)
  }

  handleMultiSelect(item: { [key: string]: any }): void {
    let values = []
    if (this.multiple) {
      if (!this.isInList(item)) {
        this.selectedListItems.push(item)
      } else {
        this.selectedListItems = this.selectedListItems.filter(value => value[this.itemValue] !== item[this.itemValue])
      }
      values = this.selectedListItems.map(item => item[this.itemValue])
      this.onChange(values)

    }
  }

  onSelect(item: T) {
    this.selectedItem = item
    this.handleMultiSelect(item)
    if (!this.multiple) {
      this.showMenu = false
      if (this.returnObject) {
        this.onChange(item)

      } else {
        if (this.itemValue) {
          this.onChange(item[this.itemValue])
        } else {
          this.onChange(item)

        }
      }
    }

  }
  onResetSelectedValue() {
    if (!this.search && this.selectedItem && !this.multiple) {
      this.selectedItem = null
      this.onChange(undefined)
    }
  }

  get selectValue(): string {
    if (!this.multiple) {
      return this.selectedItem && this.selectedItem[this.itemKey]
    } else {
      return ''
    }
  }
  get selectedListValue(): string {
    if (this.itemListByItemValue && this.itemListByItemValue.length > 0) {
      this.selectedListItems = this.items.filter(el => this.itemListByItemValue.includes(el[this.itemValue]))

    }
    return this.selectedListItems.map(item => item[this.itemKey]).join(' , ')
  }
  activeSelect(item: any) {
    if (!this.multiple) {
      return this.selectedItem && this.selectedItem[this.itemValue] === item[this.itemValue] ? 'bg-slate-200' : 'bg-white'
    } else {
      return this.isInList(item) ? 'bg-slate-200' : 'bg-white'
    }

  }

  @HostListener('document:click', ['$event'])
  onClickOutside(event: Event) {
    if (!this._element.nativeElement.contains(event.target as Node)) {
      this.showMenu = false; // Click outside, so close the menu
      this.menu.nativeElement.scrollTo({ top: 0, behavior: 'smooth' })
    }
  }
}
