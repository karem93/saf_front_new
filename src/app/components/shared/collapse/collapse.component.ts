import { Component, Input } from '@angular/core';
import { IQuestion } from 'app/pages/common-questions/type';

@Component({
  selector: 'app-collapse',
  standalone: true,
  imports: [],
  templateUrl: './collapse.component.html',
  styleUrl: './collapse.component.scss'
})
export class CollapseComponent {
  active_item: number | null = null
  @Input() items: IQuestion[] = []
  toggle(index: number) {
    if (this.active_item === index) {
      this.active_item = null
    } else {
      this.active_item = index

    }


  }

  isOpen(index: number): boolean {
    return this.active_item === index
  }
}
