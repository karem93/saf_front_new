import { Component, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IOption } from './types';

@Component({
  selector: 'select-filter',
  standalone: true,
  imports: [TranslateModule],
  templateUrl: './select-filter.component.html',
  styleUrl: './select-filter.component.scss'
})
export class SelectFilterComponent<T> {
  @Input() label: string = ''
  @Input() options: IOption[] = []
  @Output() onChange: EventEmitter<IOption> = new EventEmitter()
  showMenu: boolean = false
  selectValue: IOption = { text: '', value: '' }
  constructor(private _element: ElementRef<HTMLElement>) {

  }
  toggleShow() {
    this.showMenu = !this.showMenu;
  }

  @HostListener('document:click', ['$event'])
  onClickOutside(event: Event) {
    if (!this._element.nativeElement.contains(event.target as Node)) {
      this.showMenu = false; // Click outside, so close the menu

    }
  }
  activeSelect(item: IOption) {
    return this.selectValue.value === item.value ? 'bg-slate-200' : 'bg-white'
  }

  onSelect(item: IOption) {
    this.selectValue = item
    this.onChange.emit(item)
  }
}
