import { Component, Input } from '@angular/core';
import { GoogleMapsModule, MapMarker } from '@angular/google-maps'

@Component({
  selector: 'app-google-map',
  standalone: true,
  imports: [GoogleMapsModule],
  templateUrl: './google-map.component.html',
  styleUrl: './google-map.component.scss'
})
export class GoogleMapComponent {
  @Input() center: google.maps.LatLngLiteral = {
    lat: 0,
    lng: 0
  }
  @Input() height: string = '400px'

}
