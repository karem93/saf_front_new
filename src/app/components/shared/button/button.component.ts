import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-button',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './button.component.html',
  styleUrl: './button.component.scss'
})
export class ButtonComponent {

  @Input({ alias: 'class' }) classNames!: string | string[] | Set<string> | {
    [klass: string]: any;
  } | null | undefined
  @Input() loading: boolean = false
  @Input() disabled: boolean = false


}
