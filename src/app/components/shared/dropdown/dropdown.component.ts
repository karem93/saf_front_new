import { Component, ElementRef, HostListener } from '@angular/core';

@Component({
  selector: 'app-dropdown',
  standalone: true,
  imports: [],
  templateUrl: './dropdown.component.html',
  styleUrl: './dropdown.component.scss'
})
export class DropdownComponent {
  showMenu: boolean = false;

  onToggleMenu() {
    this.showMenu = !this.showMenu;
  }
  constructor(private _element: ElementRef<HTMLElement>) { }
  @HostListener('document:click', ['$event'])
  onClickOutside(event: Event) {
    if (!this._element.nativeElement.contains(event.target as Node)) {
      this.showMenu = false; // Click outside, so close the menu
    }
  }
}
