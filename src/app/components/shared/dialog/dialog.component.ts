import { NgTemplateOutlet } from '@angular/common';
import { Component, ContentChild, TemplateRef } from '@angular/core';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-dialog',
  standalone: true,
  imports: [NgTemplateOutlet],
  templateUrl: './dialog.component.html',
  styleUrl: './dialog.component.scss'
})
export class DialogComponent {
  dialog: boolean = false
  @ContentChild('dialogContent') dialogContent!: TemplateRef<any>;
  toggleDialog() {
    this.dialog = !this.dialog;
    if (this.dialog) {
      document.body.style.overflowY = 'hidden';
    } else {
      document.body.style.overflowY = 'auto';

    }

  }
}
