import { Component } from '@angular/core';

import { slideYAnimation } from 'app/animations/slideY';
import { ErrorService } from 'app/store/error.service';

@Component({
  selector: 'snack-bar',
  standalone: true,
  imports: [],
  templateUrl: './snack-bar.component.html',
  styleUrl: './snack-bar.component.scss',
  animations: [slideYAnimation]
})
export class SnackBarComponent {
  constructor(private errorService: ErrorService) { }



  get errorMessage() {
    return this.errorService.error().message
  }
  get errorColor() {
    return this.errorService.error().color
  }
  get showMessage(): boolean {
    return this.errorService.show

  }
}
