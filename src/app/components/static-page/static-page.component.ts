import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { PageTitleComponent } from '../shared/page-title/page-title.component';
import { HttpClient } from '@angular/common/http';
import { IStaticData } from './types';
import { Subscription, map } from 'rxjs';
import { ShimmerLoadingDirective } from 'app/directives/shimmer-loading.directive';

@Component({
  selector: 'static-page',
  standalone: true,
  imports: [PageTitleComponent, ShimmerLoadingDirective],
  templateUrl: './static-page.component.html',
  styleUrl: './static-page.component.scss'
})
export class StaticPageComponent implements OnInit, OnDestroy {
  @Input() id!: number
  loading: boolean = true
  staticData: IStaticData = {
    title: '',
    content: ''
  }
  unSub!: Subscription
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.unSub = this.http.get<{ data: IStaticData }>(`staticPages/${this.id}`).pipe(map(res => res.data)).subscribe(data => {
      this.staticData = data
      this.loading = false
    })
  }
  ngOnDestroy(): void {
    if (this.unSub) {
      this.unSub.unsubscribe()
    }
  }

}
