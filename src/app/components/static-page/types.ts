export interface IStaticData {
  title: string;
  content: string
}
