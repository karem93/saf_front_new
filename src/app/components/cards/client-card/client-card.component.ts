import { Component, Input } from '@angular/core';
import { ClientReview } from './model';

@Component({
  selector: 'client-card',
  standalone: true,
  imports: [],
  templateUrl: './client-card.component.html',
  styleUrl: './client-card.component.scss'
})
export class ClientCardComponent {
  @Input() client!: ClientReview
}
