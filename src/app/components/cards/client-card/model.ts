export interface ClientReview {
  created_at: string
  id: number
  img: string
  job_name: string
  message: string
  name: string
}
