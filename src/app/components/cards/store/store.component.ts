import { Component, Input } from '@angular/core';
import { StoreType } from '../../../store/types';
import { CommonModule } from '@angular/common';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'store-card',
  standalone: true,
  imports: [CommonModule, RouterLink],
  templateUrl: './store.component.html',
  styleUrl: './store.component.scss'
})
export class StoreCardComponent {
  @Input() item!: StoreType
}
