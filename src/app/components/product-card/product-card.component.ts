import { CommonModule } from '@angular/common';
import { Component, Input, ViewChild } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { Product } from 'app/store/types';
import { ButtonComponent } from '../shared/button/button.component';
import { DialogComponent } from '../shared/dialog/dialog.component';
import { ProductDetailsComponent } from '../product-details/product-details.component';
import { TCurrency } from '../top-bar/model';

@Component({
  selector: 'product-card',
  standalone: true,
  imports: [TranslateModule, CommonModule, ButtonComponent, DialogComponent, ProductDetailsComponent],
  templateUrl: './product-card.component.html',
  styleUrl: './product-card.component.scss'
})
export class ProductCardComponent {
  @Input() product!: Product;
  @ViewChild(DialogComponent) dialog!: DialogComponent
  textMaxLength: number = 150


  showDialog() {
    this.dialog.toggleDialog()
  }
  readMore() {
    this.textMaxLength = this.product.bio.length
  }
  get canReaMore(): boolean {
    return this.product.bio?.length > this.textMaxLength
  }
  get productBio(): string {
    if (this.canReaMore) {
      return this.product.meta_description.substring(0, this.textMaxLength) + '...'

    } else {
      return this.product.meta_description
    }
  }

  get selectedCurrency(): TCurrency {
    return (localStorage.getItem('currency') || 'SAR') as TCurrency
  }
  get price() {
    return this.product[`${this.selectedCurrency}_price`]
  }
  get priceAfter() {
    return this.product[`${this.selectedCurrency}_price_after`]
  }
  get discount() {
    return this.product[`${this.selectedCurrency}_discount`]
  }



}
