import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { IProductDetail, Status } from "./types";

@Injectable({ providedIn: 'root' })
export class ProductDetailsService {
  constructor(private http: HttpClient) { }

  fetchProductDetails(id: number) {
    return this.http.get<{ data: IProductDetail }>(`product_details/${id}`)
  }
  handleStatus(data: { status: Status, product_id: number }) {
    return this.http.post('likeStatus', data)
  }
}
