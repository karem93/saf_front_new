import { Component, EventEmitter, Input, OnChanges, Output, inject } from '@angular/core';
import { ProductDetailsService } from './productDetails.service';
import { IProductDetail, Status, StatusLoading } from './types';
import { LoaderComponent } from '../shared/loader/loader.component';
import { SectionTitleComponent } from '../shared/section-title/section-title.component';
import { SliderComponent } from '../shared/slider/slider.component';
import { ButtonComponent } from '../shared/button/button.component';
import { BreakPoint } from '../shared/slider/model';
import { TranslateModule } from '@ngx-translate/core';
import { catchError, of, tap } from 'rxjs';
import { TCurrency } from '../top-bar/model';

@Component({
  selector: 'product-details',
  standalone: true,
  imports: [LoaderComponent, SectionTitleComponent, SliderComponent, ButtonComponent, TranslateModule],
  templateUrl: './product-details.component.html',
  styleUrl: './product-details.component.scss'
})
export class ProductDetailsComponent implements OnChanges {
  activeIndex = 0
  statusLoading: StatusLoading = {
    dislike: false,
    liked: false,
    not_exists: false,
  }
  @Input() id!: number;
  @Input() dialog: boolean = false;
  @Output() onClose: EventEmitter<() => void> = new EventEmitter();

  breakPoints: BreakPoint = {
    320: {
      slidesPerView: 2,
      spaceBetween: 5,
    },
    480: {
      slidesPerView: 2,
      spaceBetween: 5,

    },
    1200: {
      slidesPerView: 4,
      spaceBetween: 10,

    },

  }
  productDetailsService: ProductDetailsService = inject(ProductDetailsService)
  product: IProductDetail = {
    id: 0,
    bio: '',
    meta_description: '',
    cover_img: '',
    SAR_discount: 0,
    dislikes: 0,
    file: '',
    images: [],
    is_liked: 'liked',
    name: '',
    price: 0,
    price_after: 0,
    views: 0,
    SAR_price: 0,
    SAR_price_after: 0,
    USD_discount: 0,
    USD_price: 0,
    USD_price_after: 0
  }
  loading: boolean = true;
  handleClose() {
    this.onClose.emit()
    this.activeIndex = 0
  }
  ngOnChanges(): void {
    if (this.dialog) {
      this.loading = true
      this.productDetailsService.fetchProductDetails(this.id).subscribe(res => {
        const { data } = res
        this.product = data
        this.loading = false
      })
    }

  }

  handleStatus(status: Status) {
    this.product.is_liked = status
    this.statusLoading[status] = true;
    const data = {
      status,
      product_id: this.product.id
    }
    this.productDetailsService.handleStatus(data)
      .pipe(
        catchError(err => {
          if (err) {
            this.statusLoading[status] = false
          }
          return of(err)
        }),
        tap(event => {
          if (event.status) {
            this.statusLoading[status] = false
            // this.handleClose()
          }
        })
      )
      .subscribe()

  }
  get selectedCurrency(): TCurrency {
    return (localStorage.getItem('currency') || 'SAR') as TCurrency
  }
  get price() {
    return this.product[`${this.selectedCurrency}_price`]
  }
  get priceAfter() {
    return this.product[`${this.selectedCurrency}_price_after`]
  }
  get discount() {
    return this.product[`${this.selectedCurrency}_discount`]
  }
  get likedBtnStyle(): string {
    return 'w-full rounded-[100px] btn-outline'
  }

}
