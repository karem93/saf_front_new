export type Status = "not_exists" | "liked" | "dislike";
export type StatusLoading = {
  [key in Status]: boolean;
};

export interface IProductDetail {
  id: number;
  bio: string;
  cover_img: string;

  dislikes: number;
  file: string;
  images: { img: string }[];
  is_liked: Status
  name: string;
  price: number;
  price_after: number
  views: number
  meta_description: string
  SAR_price: number;
  SAR_price_after: number;
  SAR_discount: number;
  USD_discount: number;
  USD_price: number;
  USD_price_after: number;
}


