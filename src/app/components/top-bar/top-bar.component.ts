import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ContactLinkType, ICurrency, ILang, SocialType, TCurrency } from './model';
import { DividerComponent } from '../shared/divider/divider.component';
import { HomeService } from '../../store/home.service';
import { TranslationService } from 'assets/i18n/translation.service';
import { SelectComponent } from '../shared/select/select.component';
import { DropdownComponent } from '../shared/dropdown/dropdown.component';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

@Component({
  selector: 'top-bar',
  standalone: true,
  imports: [DividerComponent, DropdownComponent, CommonModule, TranslateModule],
  templateUrl: './top-bar.component.html',
  styleUrl: './top-bar.component.scss'
})
export class TopBarComponent implements AfterViewInit {
  socials: SocialType[] = []
  contactLinks: ContactLinkType[] = []

  constructor(public homeService: HomeService, private transService: TranslationService) {

  }
  ngAfterViewInit(): void {
    this.homeService.contactInfo$.subscribe(data => {
      this.socials = [
        { icon: 'fa-snapchat', value: data.snapchat_link },
        { icon: 'fa-youtube', value: data.youtube_link },
        { icon: 'fa-instagram', value: data.instagram_link },
        { icon: 'fa-twitter', value: data.twitter_link },
        { icon: 'fa-facebook-f', value: data.facebook_link },
      ]
      this.contactLinks = [
        { icon: "fa-envelope", value: data.contact_email, href: `mailto:${data.contact_email}` },
        { icon: "", value: '', href: '' },
        { icon: "fa-phone", value: data.phone, href: `tel:${data.phone}` },
      ]
    })
  }
  get lang() {
    return this.transService.currentLang == 'ar' ? 'en' : 'ar' as const
  }
  changeLang() {
    this.transService.changeLanguage(this.lang)
    this.handleRefresh()
  }
  changeCurrency(currency: TCurrency) {
    localStorage.setItem('currency', currency)
    this.handleRefresh()
  }
  handleRefresh() {
    window.location.reload()

  }

  get languages(): ILang[] {
    return [
      { text: this.transService.getTransValue('arabic'), flag: '/assets/images/ar_flag.svg', locale: 'ar' },
      { text: this.transService.getTransValue('english'), flag: '/assets/images/en_flag.svg', locale: 'en' }
    ]
  }
  get currencies(): ICurrency[] {
    return [
      { text: this.transService.getTransValue('SAR'), value: 'SAR' },
      { text: this.transService.getTransValue('USD'), value: 'USD' }
    ]
  }
  get currentLang() {
    return this.languages.find(el => el.locale == this.transService.currentLang) as ILang
  }
  get activeCurrency(): TCurrency {
    return (localStorage.getItem('currency') || 'SAR') as TCurrency
  }
  get currentCurrency() {
    return this.currencies.find(el => el.value = this.activeCurrency)
  }

}
