type TLocale = 'ar' | 'en'
export type TCurrency = 'USD' | 'SAR'
export interface SocialType {
  value: string;
  icon: string
}


export interface ContactLinkType extends SocialType {
  href: string
}

export interface ILang {
  text: string;
  flag: string;
  locale: TLocale
}

export interface ICurrency {
  text: string;
  value: TCurrency
}
