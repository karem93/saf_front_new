import { CommonModule } from '@angular/common';
import { Component, OnDestroy } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { SectionTitleComponent } from '../../shared/section-title/section-title.component';
import { HomeService } from '../../../store/home.service';
import { Subscription } from 'rxjs';
import { StaticPage } from '../../../store/types';
import { ButtonComponent } from '../../shared/button/button.component';

@Component({
  selector: 'app-about-us',
  standalone: true,
  imports: [TranslateModule, CommonModule, SectionTitleComponent, ButtonComponent],
  templateUrl: './about-us.component.html',
  styleUrl: './about-us.component.scss'
})
export class AboutUsComponent implements OnDestroy {
  staticpage: Pick<StaticPage, 'title' | 'content'> = {
    title: '',
    content: ""


  }
  unSub!: Subscription
  truncatedTextLength: number = 100
  constructor(public homeService: HomeService) {
    this.homeService.homeData$.subscribe(data => {
      this.staticpage = data.staticpage
    })
  }

  get content(): string {
    return this.staticpage?.content?.substring(0, this.truncatedTextLength)
  }
  get showMore(): boolean {
    return this.staticpage?.content?.length > this.truncatedTextLength
  }

  handleShowMore() {
    this.truncatedTextLength = this.staticpage?.content?.length
  }

  ngOnDestroy(): void {
    if (this.unSub) {
      this.unSub.unsubscribe()
    }
  }
}
