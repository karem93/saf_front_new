import { Component, EventEmitter, Input, Output } from '@angular/core';
import { StoreCardComponent } from '../../cards/store/store.component';
import { CommonModule } from '@angular/common';
import { HomeService } from '../../../store/home.service';
import { StoreType } from '../../../store/types';
import { ButtonComponent } from '../../shared/button/button.component';
import { RouterLink } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { SectionTitleComponent } from '../../shared/section-title/section-title.component';
import { MetaOption } from 'app/pages/sections/types';

@Component({
  selector: 'app-stores',
  standalone: true,
  imports: [StoreCardComponent, CommonModule, ButtonComponent, RouterLink, TranslateModule, SectionTitleComponent],
  templateUrl: './stores.component.html',
  styleUrl: './stores.component.scss'
})
export class StoresComponent {
  @Input() items!: StoreType[]
  @Input() sectionTitle: string = 'latest_store'
  @Input() seeMore: boolean = true
  @Input() loading: boolean = true
  @Input() metaOption!: MetaOption
  @Output() onLoadMore: EventEmitter<() => void> = new EventEmitter();


  loadMore() {
    this.onLoadMore.emit()
  }


}
