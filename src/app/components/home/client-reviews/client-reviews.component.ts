import { CommonModule } from '@angular/common';
import { Component, OnDestroy, ViewChild } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { SectionTitleComponent } from '../../shared/section-title/section-title.component';
import { ButtonComponent } from '../../shared/button/button.component';
import { HomeService } from '../../../store/home.service';
import { Review } from '../../../store/types';
import { Subscription } from 'rxjs';
import { ClientCardComponent } from '../../cards/client-card/client-card.component';
import { SliderComponent } from '../../shared/slider/slider.component';
import { BreakPoint } from '../../shared/slider/model';
import { ClientReview } from '../../cards/client-card/model';
import { TranslationService } from 'assets/i18n/translation.service';

@Component({
  selector: 'app-client-reviews',
  standalone: true,
  imports: [TranslateModule, CommonModule, SectionTitleComponent, ButtonComponent, ClientCardComponent, SliderComponent],
  templateUrl: './client-reviews.component.html',
  styleUrl: './client-reviews.component.scss'
})
export class ClientReviewsComponent implements OnDestroy {
  reviews!: Review[]
  unSub!: Subscription
  @ViewChild(SliderComponent) sliderComponent!: SliderComponent<ClientReview>
  breakPoints: BreakPoint = {
    320: {
      slidesPerView: 1,
      spaceBetween: 16,
    },
    480: {
      slidesPerView: 1,
      spaceBetween: 16,

    },
    1200: {
      slidesPerView: 2,
      spaceBetween: 16,

    },

  }
  constructor(public homeService: HomeService, private transService: TranslationService) {
    this.unSub = this.homeService.homeData$.subscribe(data => {
      this.reviews = data.reviews
    })
  }
  handleNext() {
    this.sliderComponent.handleNext()
  }
  handlePrev() {
    this.sliderComponent.handlePrev()

  }
  ngOnDestroy(): void {
    this.unSub.unsubscribe()
  }

  get lang() {
    return this.transService.lang
  }
}
