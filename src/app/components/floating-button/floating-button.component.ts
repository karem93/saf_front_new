import { Component } from '@angular/core';

@Component({
  selector: 'floating-button',
  standalone: true,
  imports: [],
  templateUrl: './floating-button.component.html',
  styleUrl: './floating-button.component.scss'
})
export class FloatingButtonComponent {

  scrollTo() {
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    })
  }
}
