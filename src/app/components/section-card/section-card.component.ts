import { Component, Input } from '@angular/core';
import { ISection } from 'app/pages/sections/types';

@Component({
  selector: 'section-card',
  standalone: true,
  imports: [],
  templateUrl: './section-card.component.html',
  styleUrl: './section-card.component.scss'
})
export class SectionCardComponent {
  @Input() item!: ISection
}
