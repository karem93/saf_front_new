import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./routes/main.routes').then(
        (m) => m.mainRoutes
      ),
  },
  {
    path: '',
    loadChildren: () =>
      import('./routes/payment.routes').then(
        (m) => m.paymentRoutes
      ),
  },
  {
    path: '**',
    loadComponent: () =>
      import('app/pages/not-found/not-found.component').then(
        (m) => m.NotFoundComponent
      ),
  },


];
