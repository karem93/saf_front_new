/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {
      container: {
        center: true,
        padding: '1rem'
      },
      colors: {
        primary: '#24ad53',
        info: '#4285F4',
        secondary: "#1c2733",
        orange: '#ffb847',
        gray: '#80848a',
        'gray-200': '#E0E0E0',
        'gray-300': '#4F4F4F',
        'gray-400': '#9f9f9f',
        'gray-500': '#D1D1D6',
        'light-gray': '#f1f3f8'
      },

    },
  },
  plugins: [],
}

